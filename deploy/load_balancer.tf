resource "aws_lb" "api" {
  name               = "${local.prefix}-main"
  load_balancer_type = "application"
  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id
  ]

  security_groups = [aws_security_group.lb.id]

  tags = local.common_tags
}
# Group of servers that the load balance can forward requests to
resource "aws_lb_target_group" "api" {
  name     = "${local.prefix}-api"
  protocol = "HTTP"
  # Load balancer target group in the same VPC as our application
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
  # Port proxy will run on in ECS task
  port = 8000
  # Health check: feature of load balancers that perform regular polls on our application to ensure its running
  health_check {
    path = "/admin/login/"
  }
}

# Accepts the requests to our load balancer
resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.api.arn
  port              = 80
  protocol          = "HTTP"
  # How we handle requests to our listener
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }
}

resource "aws_security_group" "lb" {
  description = "Allow access to Application Load Balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.main.id
  # Accepts all connections from the public internet in to our load balancer
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Access our load balancer has to our application
  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}
